#include <stdio.h>
#include <stdlib.h>

 void pedir_entero(int *variable){

  printf("Escribe un numero entero: \n");
  scanf(" %d", variable);

}

int main() {
  int memoria;
  pedir_entero(&memoria);
  printf("%d\n",memoria);
  return EXIT_SUCCESS;
}