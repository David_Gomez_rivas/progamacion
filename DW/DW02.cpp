#include <stdio.h>
#include <stdlib.h>

int main()
{
    int numero, contador;
	float media;
		
	do
	{
		printf("Introduce un numero positivo. \n");
        scanf(" %d", &numero);
		if(numero>0){media += numero;
		contador++;}
	}while (numero>0);

	printf("Tu media es de : %f\n",media/contador);

    return EXIT_SUCCESS;
}
