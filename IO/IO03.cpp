#include <stdio.h>
#include <stdlib.h>

#define   MAX   128
#define   MIN   32

int main() {
  int numero;
   printf("Introduce el numero: ");
   scanf("%d",&numero);
   if(numero<MIN || numero>MAX) {
   printf("El numero no esta entre 32 y 128\n");
   }else{
   printf("numero como int: %d\n",numero);
   char numerochar[3];
   sprintf(numerochar, "%d", numero);
   printf("numero como char: %s\n",numerochar);
   }
 return EXIT_SUCCESS;
}
