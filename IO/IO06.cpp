#include <stdio.h>
#include <stdlib.h>

#define CYAN_T     "\x1b[36m"
#define CYAN_F     "\x1b[46m"
#define VERDE_T    "\x1b[32m"
#define VERDE_F    "\x1b[42m"

int main() {
  int numero1, numero2, resultado;
   printf("Introduce el primer numero: ");
   scanf("%d",&numero1);
   printf("Introduce el segundo numero: ");
   scanf("%d",&numero2);
   resultado = numero1 + numero2;
   printf(CYAN_T " %d+%d=" VERDE_T "%d\n",numero1,numero2,resultado);
 return EXIT_SUCCESS;
}
