#include <stdio.h>
#include <stdlib.h>
#include <string.h>

 int main(){
  int numero;
  int *direccion;

  numero = 5;

  direccion = &numero;
  printf("El numero numero: %d\n", numero);

  printf("Tiene esta direccion de memoria: %p\n", direccion);

  return EXIT_SUCCESS;
}
