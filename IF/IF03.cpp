#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main() {
  char respuesta;
  printf("Quieres instalar algo: s/n \n");
  scanf("%c", &respuesta);
  respuesta = tolower(respuesta);
  if (respuesta == 's') {
    printf("Pues instalalo\n");
  } else {
    printf("Pues no lo instales\n");
  }

    return EXIT_SUCCESS;
}

