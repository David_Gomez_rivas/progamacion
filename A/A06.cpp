#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
  
  int A[3][10];
    
  for ( int i = 0; i < 3; i++){
     for ( int j = 0; j < 10; j++){
      
      A[i][j]=pow(j,i+1);
      printf("%d ",A[i][j]);
    }
    printf("\n");
   }

    return EXIT_SUCCESS;
}
