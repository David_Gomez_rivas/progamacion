#include <stdio.h>
#include <stdlib.h>

int main()
{
    char str[100];
    int i = 0;
        
    printf("\n Introduce una palabra: ");
    scanf("%s", str);
        
    while (str[i] != '\0')
    {
        printf("En la posicion %d el caracter es el: %c \n", i, str[i]);
        i++;
    }

    return EXIT_SUCCESS;
}
