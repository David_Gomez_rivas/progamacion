#include <iostream>
#include <string>
#include <algorithm>

int main()
{
    std::string str = "De principio a fin";
    auto begin = str.begin();
    while (begin != str.end())
    {
        auto end = std::find(begin, str.end(), ' ');
        std::reverse(begin, end);
        if (end == str.end())
        {
            break;
        }
        begin = end + 1;
    }
    std::cout << str << "\n";

    return EXIT_SUCCESS;
}

